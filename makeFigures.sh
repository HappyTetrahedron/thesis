#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd "$DIR"

mkdir -p figures
mkdir -p tables

if [[ -z $1 ]]
then
    for framework in tf-dr-n tf-ps-n cntk tf-ps-best cntk-strong tf-ps-best-strong hvd hvd-1024 cntk-1024
    do
        echo "Making plots for $framework..."
        for method in cibar cibar-nooutliers
        do
            echo "    $method..."
            echo "        All in one..."
            scripts/$method.py -f tests/$framework/*-imgpersec.data -o figures/ -F pdf 
            echo "        AV..."
            scripts/$method.py -f tests/$framework/*-imgpersec.data -o figures/ -F pdf -p alexnet,vgg19,AlexNet,VGG19 -s av
            echo "        RI..."
            scripts/$method.py -f tests/$framework/*-imgpersec.data -o figures/ -F pdf -p resnet50,resnet152,inception3,ResNet_50,ResNet_152,BN-Inception -s ri -j
        done
        for method in cistack cistack-nooutliers
        do
            echo "    $method..."
            echo "        All in one..."
            scripts/$method.py -f  tests/$framework/*-compcomm.data -o figures/ -F pdf 
            echo "        AV..."
            scripts/$method.py -f  tests/$framework/*-compcomm.data -o figures/ -F pdf -p alexnet,vgg19,AlexNet,VGG19 -s av
            echo "        RI..."
            scripts/$method.py -f  tests/$framework/*-compcomm.data -o figures/ -F pdf -p resnet50,resnet152,inception3,ResNet_50,ResNet_152,BN-Inception -s ri -j
        done
        echo "    fitfun..."
        scripts/curvefit.py -f tests/$framework/*-compcomm.data -T log -i 1 -o figures/ -O tables/ -F pdf -s comm-fit -t "Communication time model fitting for $framework" 
        scripts/curvefit.py -f tests/$framework/*-compcomm.data -T log -i 1 -o figures/ -O tables/ -F pdf -s comm-nolegend -t "Communication time model fitting for $framework" -j
    done
fi

if [[ -z $2 ]]
then
    echo "Making strong scaling fitfun plots..."
    for framework in cntk-strong
    do
         scripts/curvefit.py -f tests/$framework/*-compcomm.data -T inverse -i 0 -o figures/ -O tables/ -F pdf -s comp-fit -t "Computation time model fitting for $framework" 
         scripts/curvefit.py -f tests/$framework/*-compcomm.data -T inverse -i 0 -o figures/ -O tables/ -F pdf -s comp-nolegend -t "Computation time model fitting for $framework" -j
    done


    echo "Making comparison plots..."

    echo "    Images per second..."
    for comparison in tests/compare-imgpersec-*.data
    do
        echo "        $comparison..."
        scripts/cibar.py -f "$comparison" -o figures/ -F pdf
        scripts/cibar-nooutliers.py -f "$comparison" -o figures/ -F pdf
    done

    echo "    Comp/comm times..."
    for comparison in tests/compare-compcomm-*.data
    do
        echo "        $comparison..."
        scripts/cistack.py -f "$comparison" -o figures/ -F pdf
        scripts/cistack-nooutliers.py -f "$comparison" -o figures/ -F pdf
    done

    echo "    Comm times..."
    for comparison in tests/compare-compcomm-*cntk*weak?vs?strong*.data
    do
        echo "        $comparison..."
        scripts/cistack.py -f "$comparison" -o figures/ -F pdf -i 1 -s comm
    done
fi

echo "Making special snowflake plots..."

scripts/cistack.py -f  tests/cntk/*-compcomm.data -o figures/ -F pdf -p ResNet_50 -s resnet50 -j
scripts/cistack-nooutliers.py -f  tests/cntk/*-compcomm.data -o figures/ -F pdf -p ResNet_50 -s resnet50

scripts/cistack.py -f  tests/cntk/*-compcomm.data -o figures/ -F pdf -p VGG19 -s vgg19 -j
scripts/cistack-nooutliers.py -f  tests/cntk/*-compcomm.data -o figures/ -F pdf -p VGG19 -s vgg19

scripts/cistackall.py -f  tests/cntk/*-compcomm.data -o figures/ -F pdf

scripts/cibarall.py -f tests/compare-imgpersec-resnet50-strong.data -o figures/ -F pdf
scripts/cibarall.py -f tests/compare-imgpersec-vgg19-strong.data -o figures/ -F pdf
