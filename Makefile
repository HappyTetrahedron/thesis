# say 'make V=1' to see the full build output
ifndef V
Q = @
Q_LATEX = sh -c 'echo -e "   " `echo $$(basename "$$0") | tr a-z A-Z`"\t$${!\#/..\/..\//}"; ! "$$0" "$$@" | grep -A5 -E "^[^ ]+:[0-9]+:"'
Q_OTHER = sh -c 'echo -e "   " `echo $$(basename "$$0") | tr a-z A-Z`"\t$${!\#/..\/..\//}"; "$$0" "$$@" >/dev/null'
endif
  
LATEX = texi2pdf

default: all

all: thesis

thesis: data figure table pdf

data:
	tests/gatherData.sh

figure:
	./makeFigures.sh

table:
	./makeTables.sh


pdf: figures report/thesis.tex
	${Q} cd report && ${Q_LATEX} $(LATEX) thesis.tex 
	${Q} cd report && ${Q_LATEX} $(LATEX) thesis.tex 
	${Q} cd report && ${Q_LATEX} $(LATEX) thesis.tex 

clean:
	rm -rf figures/
	rm -f tests/*/*.data
	rm -f tests/*.data
	rm -f report/*.log
	rm -f report/thesis.pdf
	rm -f report/thesis.aux
	rm -f report/thesis.bbl
	rm -f report/thesis.blg
	rm -f report/thesis.out
	rm -f report/thesis.thm
	rm -f report/thesis.toc
