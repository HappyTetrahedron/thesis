#!/usr/bin/env python3

# Plots a grouped stacked bar graph for arbitrary data supplied as a yaml file.
# The data is structured as follows:
#
# meta:
#  xlabel: "My x axis"
#  ylabel: "My y axis"
#  legendsuffix: "Bar"
#  segmentlabels:
#    - "label 1"
#    - "label 2"
# group1:
#   bar1:
#     - value1
#     - value2
#   bar2:
#     - value1
#     - value2
# group2:
#   bar1:
#     - value1
#     - value2
#   bar2:
#     - value1
#     - value2
#
# Bars within a group consist of as many segments as there are values.
# There have to be the same number of values for every bar, and the same number of bars for every group.
# The 'meta' section contains some parameters to customize the plot, such as axis labels.
#

import numpy
import matplotlib.pyplot as plt
import matplotlib.patches as mpat
import common

suffix = "box"

def set_box_colors(bp, colormap):
    for i,col in enumerate(colormap):
        try:
            plt.setp(bp['boxes'][i], color=col)
            plt.setp(bp['caps'][2*i], color=col)
            plt.setp(bp['caps'][2*i+1], color=col)
            plt.setp(bp['whiskers'][2*i], color=col)
            plt.setp(bp['whiskers'][2*i+1], color=col)
            plt.setp(bp['fliers'][2*i], color=col)
            plt.setp(bp['fliers'][2*i+1], color=col)
            plt.setp(bp['medians'][i], color=col)
        except IndexError:
            pass

opts = common.get_options()
(metadata, data) = common.get_data(opts)

fig = plt.figure()
ax = fig.add_axes((.1,.4,.8,.5))

# List of colors to be used for bars within a group
colormap = common.get_colormap()

ticks = []
ticklabels = []

x_low = 0
x_high = 0

# Iterate over all groups
for i,group in enumerate(sorted(data.keys())):
    ys = list(data[group].values())

    # Number of segments per bar
    nstack = len(ys)

    # Number of bars in this group
    nbars = len(ys)

    # Width of a single bar
    width = (1./(nbars + 1.))*0.6

    # List of x positions for each bar
    x = numpy.linspace(i,i+1,nbars + 2)[:-2]

    # Plot box plot
    bp = ax.boxplot(ys, positions=x, widths=width, notch=True)
    set_box_colors(bp, colormap)

    # Add a single tick to the x axis representing this entire group
    ticks += [numpy.average(x)]

    x_low = min(x_low, min(x) - width)
    x_high = max(x_high, max(x) + width)


# Set up x axis labels
ax.set_xticks(ticks)
ax.set_xticklabels(sorted(data.keys()))

if 'title' in metadata:
    ax.set_title(metadata['title'])

# Set up axis labels as specified in the data
ax.set_ylabel(metadata.get('ylabel') if metadata != None else '')
ax.set_xlabel(metadata.get('xlabel') if metadata != None else '') 

ax.set_xlim(x_low, x_high)

if not opts.hidelegends:
# create proxy artists to use as "fake" legend entries for the bars
# This is essentially a list of "patches", each with a color and a string label
    bar_legend_handles = [mpat.Patch(color=color, label=str(indices) + " " + str(metadata.get('legendsuffix') if metadata != None else '') ) for indices,color in zip(list(data.values())[0].keys(), colormap)]

# create bar legend
    bar_legend = ax.legend(handles=bar_legend_handles, loc='center left', bbox_to_anchor=(1,0.5), fancybox=True)

common.save(opts, suffix, plt)
