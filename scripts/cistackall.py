#!/usr/bin/env python3

# Plots a grouped stacked bar graph for arbitrary data supplied as a yaml file.
# The data is structured as follows:
#
# meta:
#  xlabel: "My x axis"
#  ylabel: "My y axis"
#  legendsuffix: "Bar"
#  segmentlabels:
#    - "label 1"
#    - "label 2"
# group1:
#   bar1:
#     segment1:
#       - value1
#       - value2
#     segment2:
#       - value1
#       - value2
#   bar2:
#     segment1:
#       - value1
#       - value2
#     segment2:
#       - value1
#       - value2
# group2:
#   bar1:
#     segment1:
#       - value1
#       - value2
#     segment2:
#       - value1
#       - value2
#   bar2:
#     segment1:
#       - value1
#       - value2
#     segment2:
#       - value1
#       - value2
#
# Bars within a group consist of as many segments as there are segment sections.
# There have to be the same number of segments for every bar, and the same number of bars for every group.
# The 'meta' section contains some parameters to customize the plot, such as axis labels.
#

import common
import numpy
import math
import matplotlib.pyplot as plt
import matplotlib.patches as mpat

suffix = "cistackbarlets"

opts = common.get_options()
(metadata, data) = common.get_data(opts)

fig = plt.figure()
ax = fig.add_axes((.1,.4,.8,.5))

# List of colors to be used for bars within a group
colormap=common.get_colormap()

ticks = []
ticklabels = []

# Iterate over all groups
for i,group in enumerate(sorted(data.keys())):
    
    nstack = len(list(data[group].values())[0])
    nbars = len(data[group].values())

    width = 1./(nbars + 1.)
    x = numpy.linspace(i,i+1,nbars + 2)[:-2]

    last = []
    # for each segment...
    for (row, _) in enumerate(next(iter(data[group].values()))):
        if common.use_index(opts, row):
            # Get all measurements for this segments for every group
            perbar_lists = [ x[row] for x in data[group].values() if row < len(x)]

            for i,perbar_list in enumerate(perbar_lists):
                nbarlets = len(perbar_list)
                barletwidth = width/nbarlets
                barletx = numpy.linspace(x[i] - width/2 + barletwidth/2, x[i] + width/2 - barletwidth/2, nbarlets)
                color = colormap[i]

                if row == 0:
                    ax.bar(barletx, perbar_list, barletwidth, label=group, color=color, edgecolor='k', alpha=1)
                else:
                    ax.bar(barletx, perbar_list, barletwidth, label=group, color=color, alpha=1 - (row / float(nstack)), bottom=last[i], edgecolor='k')

            last = perbar_lists

    # Add a single tick to the x axis representing this entire group
    ticks += [numpy.average(x)]
    ticklabels.append(group)


# Set up x axis labels
ax.set_xticks(ticks)
ax.set_xticklabels(ticklabels)

if opts.logscale:
    ax.set_yscale('log')

if 'title' in metadata:
    ax.set_title(metadata['title'])

# Set up axis labels as specified in the data
ax.set_ylabel(metadata.get('ylabel') if metadata != None else '')
ax.set_xlabel(metadata.get('xlabel') if metadata != None else '') 

if not opts.hidelegends:
# create proxy artists to use as "fake" legend entries for the bars
# This is essentially a list of "patches", each with a color and a string label
    bar_legend_handles = [mpat.Patch(color=color, label=str(indices) + " " + str(metadata.get('legendsuffix') if metadata != None else '') ) for indices,color in zip(list(data.values())[0].keys(), colormap)]

# similarly create legend for the segments if required
    segment_legend = None
    if metadata != None and metadata.get('segmentlabels') != None:
        # proxy artists
        segmentlabels = common.filter_list(opts, metadata.get('segmentlabels'))
        segment_legend_handles = [mpat.Patch(color='k', alpha=float(i+1)/float(len(segmentlabels)), label=str(label)) for i,label in enumerate(reversed(segmentlabels))]
        # create segment legend
        segment_legend = ax.legend(handles=segment_legend_handles, fancybox=True)
        # explicitly add legend to axis so we can add a second one later
        plt.gca().add_artist(segment_legend)


# create bar legend
    bar_legend = ax.legend(handles=bar_legend_handles, loc='center left', bbox_to_anchor=(1,0.5), fancybox=True)

common.save(opts, suffix, plt)
