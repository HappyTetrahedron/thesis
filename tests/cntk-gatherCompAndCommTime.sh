#!/bin/bash

file="$1"

shopt -s extglob

rm -f $file

cat <<EOT >> $file
meta:
    ylabel: "Time per node and MB [s]"
    xlabel: ""
    legendsuffix: "Nodes"
    segmentlabels:
        - "Computation Time"
        - "Communication Time"
    tablecolumns:
        - "Comp. Time"
        - "Comm. Time"
EOT


for net in $( ls */*/!(*.rank*) | sed 's/.*\///' | sort | uniq )
do
    echo "$net:" | sed 's/Log_//' >> $file

    for numcores in $( ls */ | grep -e '^[0-9]' | sort -n | uniq )
    do
        echo "    $numcores": >> $file

        totals=()
        comms=()
        mbs=()

        for measurement in */
        do

            for log in $( ls ${measurement}$numcores/$net* )
            do
                # Total communication time for each core
                cat $log | grep 'Actual' | awk '{ SUM += $5 ; CNT += 1 } END { print SUM }' >> ${measurement}$numcores.comm
                # Total time for each core
                cat $log | grep -o 'time = [^s]*' | awk '{ SUM += $3 ; CNT += 1 } END { print SUM }' >> ${measurement}$numcores.total
            done

            # Average total communication time over all cores
            comm="$( awk '{ SUM += $1 ; CNT += 1 } END { print (CNT > 0) ? SUM/CNT : 0 }' ${measurement}$numcores.comm )"
            # Average total time over all cores
            total="$( awk '{ SUM += $1 ; CNT += 1 } END { print (CNT > 0) ? SUM/CNT : 0 }' ${measurement}$numcores.total )"

            if ! [[ $total == 0 ]]
            then
                comms+=("$comm")
                totals+=("$total")

                # Number of minibatches processed
                mbs+=("$( grep '100.00%' ${measurement}$numcores/$net | awk '{ print $6 } END { if (!NR) print "1" }' | sed -e 's/,//' )")
            fi

            rm ${measurement}$numcores.comm ${measurement}$numcores.total

        done

        echo "        - " >> $file
        
        if [[ -z "$totals" ]]
        then
            echo "            - 0" >> $file
        fi

        for i in ${!comms[@]}
        do
            # Average Computation time = (total time - communication time) / number of MB
            echo -n "            - " >> $file
            echo "(${totals[$i]} - ${comms[$i]}) / ${mbs[$i]}" | bc -l >> $file
        done

        echo "        - " >> $file
        
        if [[ -z "$comms" ]]
        then
            echo "            - 0" >> $file
        fi

        for i in ${!comms[@]}
        do
            # Average Communication time = communication time / number of MB
            echo -n "            - " >> $file
            echo "${comms[$i]} / ${mbs[$i]}" | bc -l >> $file
        done


    done

done
